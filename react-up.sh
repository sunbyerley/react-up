#!/bin/bash

BOOTSTRAP_CSS=node_modules/bootstrap/dist/css/bootstrap.min.css
BOOTSTRAP_THEME_CSS=node_modules/bootstrap/dist/css/bootstrap-theme.min.css
BOOTSTRAP_FONTS=node_modules/bootstrap/dist/fonts

DRAFT_CSS=https://raw.githubusercontent.com/facebook/draft-js/master/website/src/draft-js/css/draft.css
RICHEDIT_CSS=https://raw.githubusercontent.com/facebook/draft-js/master/examples/draft-0-10-0/rich/RichEditor.css

CSS=public/css
FONTS=public/fonts
###############################################################################
## Scaffold
###############################################################################

## Base Tree 
mkdir app
mkdir public

### main.js 
cat $1 > app/main.js <<BLOB
import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

ReactDOM.render(<App />, document.getElementById('root'));
BLOB

### App.js 
cat $1 > app/App.js <<BLOB
import React, { Component } from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import Container from './Container.js';
import Home from './Home.js';

class App extends Component {
  render() {
    return (
      <Router history={hashHistory}>
        <Route path='/' component={Container}>
          <IndexRoute component={Home} />
        </Route>
        <Route path='*' component={NotFound} />
      </Router>
    );
  }
}

const NotFound = () => <h1>404!</h1>;

export default App;
BLOB

### Container.js 
cat $1 > app/Container.js <<BLOB
import React, { Component } from 'react';
import { Link, IndexLink } from 'react-router';

class Container extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='container'>
        {this.props.children}
      </div>
    );
  }
}

export default Container;
BLOB

### Home.js 
cat $1 > app/Home.js <<BLOB
import React, { Component } from 'react';
import { Link, IndexLink } from 'react-router';

class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <h1>Home</h1>;
  }
}

export default Home;
BLOB

## Public
mkdir public
mkdir $CSS
mkdir $FONTS

## app.css 
touch $CSS/app.css

## Index.html
cat $1 > public/index.html <<BLOB
<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>React App</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="./css/draft.css">
    <link rel="stylesheet" href="./css/RichEdit.css">
    <link rel="stylesheet" href="./css/app.css">
    
  </head>
  <body>
    <div id="root"></div>
    <script src="./bundle.js"></script>
  </body>
</html>
BLOB

## Draft.css + RichEdit theme

DRAFT_PATH=public/css/draft.css
RICHEDIT_PATH=public/css/RichEdit.css

wget -O - $DRAFT_CSS > $DRAFT_PATH
wget -O - $RICHEDIT_CSS > $RICHEDIT_PATH

###############################################################################
## Configs
###############################################################################

## README.md
cat $1 > README.md <<BLOB
# React Project
BLOB

## .babelrc
cat $1 > .babelrc <<BLOB
{
  "presets": [
    "es2015",
    "react"
  ]
}
BLOB

## jsconfig.json
cat $1 > jsconfig.json <<BLOB
{
  "compilerOptions": {
    "jsx": "react",
    "module": "es6",
    "noImplicitUseStrict": true,
    "allowSyntheticDefaultImports": false,
    "target": "es5"
  }
}
BLOB

## webpack.config.js 
cat $1 > webpack.config.js <<BLOB
const path = require('path');

module.exports = {
  entry: path.join(__dirname, 'app', 'main.js'),
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  devServer: {
    inline: true,
    contentBase: path.join(__dirname, 'public'),
    port: 8100
  },
  module: {
    loaders: [
      {
        test: /\.js\$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css\$/,
        exclude: /node_modules/,
        loader: 'style-loader'
      },
      {
        test: /\.css\$/,
        exclude: /node_modules/,
        loader: 'css-loader'
      }
    ]
  }
};
BLOB

## .eslintrc.json
cat $1 > .eslintrc.json <<BLOB
{
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": "eslint:recommended",
  "parserOptions": {
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
      "jsx": true
    },
    "sourceType": "module"
  },
  "plugins": [
    "react"
  ],
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ],
    "no-unused-vars": "off",
    "no-undef": "off",
    "no-console": "off",
    "no-extra-bind": "warn"
  }
}
BLOB

## package.json
cat $1 > package.json <<BLOB
{
  "name": "react-app",
  "version": "1.0.0",
  "description": "React app",
  "main": "./app/main.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "webpack-dev-server",
    "pkg": "webpack"
  },
  "repository": {
    "type": "git",
    "url": ""
  },
  "author": "",
  "license": "",
  "homepage": "",
  "dependencies": {
    "draft-js": "^0.10.0",
    "moment": "^2.17.1",
    "moment-timezone": "^0.5.11",
    "react": "^15.4.2",
    "react-bootstrap": "^0.30.7",
    "react-dom": "^15.4.2",
    "react-http-request": "^1.0.3",
    "react-moment": "^0.1.3",
    "react-router": "^3.0.2"
  },
  "devDependencies": {
    "babel-core": "^6.22.1",
    "babel-loader": "^6.2.10",
    "babel-preset-es2015": "^6.22.0",
    "babel-preset-react": "^6.22.0",
    "bootstrap": "^3.3.7",
    "css-loader": "^0.26.1",
    "style-loader": "^0.13.1",
    "webpack": "^2.2.1",
    "webpack-dev-server": "^2.3.0"
  }
}
BLOB

# Install Packages
npm install

## Copy Bootstrap Files
cp $BOOTSTRAP_CSS $CSS
cp $BOOTSTRAP_THEME_CSS $CSS
cp $BOOTSTRAP_FONTS/* $FONTS/.

echo '~fin'

exit 0