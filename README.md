# React-UP

A shell script to scaffold an opinionated React application "stack" for development.

### Install

```
wget -O - https://bitbucket.org/sunbyerley/react-up/downloads/react-up.sh | bash -
```

---

## Batteries

### react-router
### react-bootstrap
### react-request
### react-moment
### draft.js editor

---

## Platforms

__OSX__

__Linux__

---

## Requires

__wget__

__bash__

_OSX user can install `wget` using HomeBrew._

---

## Install

```
wget -O - https://bitbucket.org/sunbyerley/react-up/downloads/react-up.sh | bash -
```
